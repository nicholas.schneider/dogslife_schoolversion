﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectspawn : MonoBehaviour {
	public CharacterControl character;
	//private float trashSpawnTime = 5f;
	//private float waterSpawnTime = 3f;
	public GameObject _trash;
	public GameObject _puddle;
	public GameObject _plant;
	public GameObject _hydrent;
	private GameObject _plantClone;
	private GameObject _hydrentClone;
	private GameObject _trashClone;
	private GameObject _trashclone1;
	private GameObject _puddleClone1;
	private GameObject _PuddleClone2;
	public GameObject _trashdown;
	

	// Use this for initialization
	void Start () {
		InvokeRepeating("trashCan",10,7);
		InvokeRepeating("PuddleClone1",3,6);
		InvokeRepeating("PuddleClone2",0,6);
		InvokeRepeating("plant", 20, 9);
		InvokeRepeating("hydrent", 10, 10);
	}
	
	// Update is called once per frame
	void Update () 
	{	
		if(character._isdown){
			_trashclone1 = Instantiate(_trashdown, new Vector3(_trashClone.transform.position.x+1,0,0),Quaternion.identity);
			Destroy(_trashClone);
			Destroy(_trashclone1,3);
			character._isdown = false;
		}	
	}
	void trashCan()
	{
		int x = Random.Range(1,4);
		if(_trashClone == null)
		{
			if(x == 1)
			{
				_trashClone = Instantiate(_trash,new Vector3(45,1,0),Quaternion.identity) as GameObject;
			}
			else if(x ==2)
			{
				_trashClone = Instantiate(_trash,new Vector3(77,1,0),Quaternion.identity) as GameObject;
			}
			else if(x == 3)
			{
				_trashClone = Instantiate(_trash,new Vector3(45,1,0),Quaternion.identity) as GameObject;
			}
			else if (x == 4)
			{
				_trashClone = Instantiate(_trash,new Vector3(45,1,0),Quaternion.identity) as GameObject;
			}
			Destroy(_trashClone,9);
		}
			
	}

	void PuddleClone1()
	{
		float x = Random.Range(4.0f,150.0f);
		if ( _puddleClone1 == null)
		{
  			_puddleClone1 = Instantiate(_puddle ,new Vector3(x,-2,0),Quaternion.identity) as GameObject;
		}
		  Destroy(_puddleClone1,5);
	}
	void PuddleClone2()
	{
		float x = Random.Range(4.0f,150.0f);
		if ( _puddleClone1 == null)
		{
  			_puddleClone1 = Instantiate(_puddle ,new Vector3(x,-2,0),Quaternion.identity) as GameObject;
		}
		  Destroy(_puddleClone1,5);
	}
	void plant()
	{
		float x = Random.Range(4.0f,149.0f);
		if ( _plantClone == null)
		{
  			_plantClone = Instantiate(_plant ,new Vector3(x,-0.5f,0),Quaternion.identity) as GameObject;
		}
		  Destroy(_plantClone,8);
	}
	void hydrent()
	{
		float x = Random.Range(4.0f,148.0f);
		if ( _hydrentClone == null)
		{
  			_hydrentClone = Instantiate(_hydrent ,new Vector3(x,0.5f,0),Quaternion.identity) as GameObject;
		}
		  Destroy(_hydrentClone,8);
	}
}
	