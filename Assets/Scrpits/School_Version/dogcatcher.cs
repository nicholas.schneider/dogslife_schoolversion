﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dogcatcher : MonoBehaviour {
	public CharacterControl dog;
	public GameObject _dogCatcher;
	public Text displaytime;
	// Use this for initialization
	void Start () {
		InvokeRepeating("MINUSTIME", 1.0f, 1.0f);
		
	}
	
	// Update is called once per frame
	void Update () {
		if(dog.timeLeft <= 0 )
		{
			CancelInvoke("MINUSTIME");
			dog.stop = true;
			Instantiate(_dogCatcher,new Vector3(dog.transform.position.x+5,1 ,0),Quaternion.identity);
			dog.over =true;

		}
		
	}

	void MINUSTIME ()
	{
		
		
			dog.timeLeft-- ;
			displaytime.text = ((int)dog.timeLeft).ToString();
		
	}
}
