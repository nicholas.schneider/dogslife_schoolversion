﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameControl : MonoBehaviour {
	public CharacterControl dog;
	public Text newscore;
	public int _score;
	public GameObject endGame;
	public GameObject winGame;
	private GameObject next;
	bool exit;
	// Use this for initialization
	void Start ()
	{
		_score =0;
		//UpdateScore();
	}
	
	// Update is called once per frame
	void Update () 
	{
		exit = false;
		if(_score >= 50)
		{
			dog.stop = true;
			next = Instantiate(winGame ,new Vector3(dog.transform.position.x-4.4f,-0.8f,0),Quaternion.identity) as GameObject;
			_score +=0;//stop the if statment
			dog.timeLeft+= 100;
			exit = true;
			
		}
		else if(dog.over && dog.timeLeft <= 0)
		{
			dog.stop = true;
			next = Instantiate(endGame ,new Vector3(dog.transform.position.x-4.4f,-0.8f,0),Quaternion.identity) as GameObject;
			dog.over = false;
			dog.timeLeft += 100;
			exit = true;
			
		}
		if(exit)
		{
			Invoke("EXIT",99);
		}
	}
	public void UpdateScore(int scoreupdate)
	{
		_score += scoreupdate;
		newscore.text = _score.ToString();
	}
	public void LOADMENU()
	{

	}
	public void LOADSCENE(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}
	void EXIT()
	{
		Application.Quit();
	}

}
	