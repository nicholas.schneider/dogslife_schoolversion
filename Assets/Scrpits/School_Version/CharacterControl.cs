﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterControl : MonoBehaviour {	
	public gameControl game;
	public Canvas display;
	public Camera cam;
	public Animator animator;
	public GameObject stombar;
	public GameObject bladbar;
	public GameObject _trash;
	public GameObject _puddle;
	public GameObject _hydrent;
	public GameObject _plant;
	private float _moveIncrement = .2f;
	private int _stoamch;
	private int _bladder;
	private float _eatTime;
	private float _drinkTime;
	private float _peeTime;
	private float _poopTime;
	public float timeLeft;
	public bool stop = false;
	private int _Score;
	private bool PLANT;
	public bool TRASH;
	private bool PUDDLE;
	private bool HYDRENT;
	public bool _isdown;
	public bool over;
	

	// Use this for initialization
	void Start () {
		cam = Camera.main;
		cam.enabled =true;
		animator = gameObject.GetComponent<Animator>();
		timeLeft = 75f;
		_bladder =0;
		_stoamch =0;
		_eatTime = 5.0f;
		_poopTime =2.0f;
		_drinkTime = 5.0f;
		_peeTime = 2.0f;
		_Score = 5;
		_bladder =0;
		_stoamch =0;
		stombar.transform.localScale = new Vector3(0,1,0);
		bladbar.transform.localScale = new Vector3(0,1,0);
		PLANT = false;
		TRASH = false;
		PUDDLE = false;
		HYDRENT = false;
		over =false;
	
		
	}
	
	// Update is called once per frame
	void Update () {
		animator.SetBool("poop",false);
		animator.SetBool("pee", false);
		animator.SetBool("eat", false);
		if(stop == false)
		{
			//moves the dog, camera, canvas to the right
			if (Input.GetKey (KeyCode.RightArrow)) 
			{
				if (transform.position.x <150) 
				{	
					if(transform.rotation.y != 0)
					{
						transform.rotation = Quaternion.Euler (0, 0, 0);
					}
					transform.position += new Vector3 (_moveIncrement, 0, 0);
					if(cam.transform.position.x <=152)
					{
					display.transform.position += new Vector3(_moveIncrement,0,0);
					cam.transform.position += new Vector3(_moveIncrement, 0, 0);
					}
				}
			}
			//moves the dog camera and canvas to the left
			else if (Input.GetKey (KeyCode.LeftArrow)) 
			{
				if (transform.position.x > -2) 
				{	
					if(transform.rotation.y != 180)
					{
						transform.rotation = Quaternion.Euler (0, 180, 0);
					}
					transform.position -= new Vector3 (_moveIncrement, 0, 0);
					if(cam.transform.position.x	>= -2)
					{
						display.transform.position -= new Vector3(_moveIncrement,0,0);
						cam.transform.position -= new Vector3(_moveIncrement, 0, 0);
					}
				}
			}
			//makes the dog poop adds 5 to score adds time and dogs speeed.
			else if (Input.GetKey (KeyCode.P) && _stoamch == 15)
			{
				 animator.SetBool("poop",true);
				 game.UpdateScore(_Score);
				 timeLeft +=_poopTime;
				 _poopTime *= 0.9f;
				 _moveIncrement += .05f;
				 _stoamch = 0;
				 stombar.transform.localScale = new Vector3(0,1,0);
				 Input.ResetInputAxes();
			}
			//make the dog pee. adds to score and the time and the dogs speed
			else if(Input.GetKey(KeyCode.U) && _bladder == 15)
			{
				if(PLANT == true || HYDRENT == true)
				{
					animator.SetBool("pee", true);
					game.UpdateScore(_Score);
					timeLeft += _peeTime;
					_peeTime *= 0.9f;
					_moveIncrement += .02f;
					_bladder = 0;
					bladbar.transform.localScale = new Vector3(0f,1,0);
					Input.ResetInputAxes();
					PLANT = false;
					HYDRENT = false;
				}
			}
			else if(Input.GetKey(KeyCode.E) && TRASH == true)
			{
				if(_stoamch <15)
				{
					_isdown = true;
					animator.SetBool("eat",true);
					_stoamch +=5;
					timeLeft +=_eatTime;
					_eatTime *=0.9f;
					game.UpdateScore(_Score);
					if(_stoamch == 5)
					{
						stombar.transform.localScale = new Vector3(0.35f,1,0);
					}
					else if(_stoamch == 10)
					{
						stombar.transform.localScale = new Vector3(.65f,1,0);
					}
					else if(_stoamch == 15)
					{
						stombar.transform.localScale = new Vector3(1f,1,0);
					}
				}
				Input.ResetInputAxes();
				TRASH = false;
			}
			else if(Input.GetKey(KeyCode.D) && PUDDLE == true)
			{
				if(_bladder < 15)
				{
					animator.SetBool("eat",true);
					_bladder +=5;
					timeLeft +=_drinkTime;
					_drinkTime *=0.9f;
					game.UpdateScore(_Score);
					if(_bladder == 5)
					{
						bladbar.transform.localScale = new Vector3(.35f,1,0);
					}
					if(_bladder == 10)
					{
						bladbar.transform.localScale = new Vector3(.65f,1,0);
					}
					if(_bladder == 15)
					{
						bladbar.transform.localScale = new Vector3(1f,1,0);
					}
				}
				Input.ResetInputAxes();
				PUDDLE = false;
			}
		}
		
			
    }
	void OnTriggerEnter2D(Collider2D trig)
	{
		if(trig.gameObject.tag == "TRASHtrig")
		{
			TRASH = true;
		}
		else if(trig.gameObject.tag == "HYDRENTtrig")
		{
			HYDRENT = true;
		}
		else if(trig.gameObject.tag == "PUDDLEtrig")
		{
			PUDDLE = true;
		}	
		else if(trig.gameObject.tag == "PLANTtrig")
		{
			PLANT = true;
		}
	}
	void OnTriggerExit2D(Collider2D trig)
	{
		if(trig.gameObject.tag == "TRASHtrig")
		{
			TRASH = false;
		}
		else if(trig.gameObject.tag == "HYDRENTtrig")
		{
			HYDRENT = false;
		}
		else if(trig.gameObject.tag == "PUDDLEtrig")
		{
			PUDDLE = false;
		}	
		else if(trig.gameObject.tag == "PLANTtrig")
		{
			PLANT = false;
		}
	}
}
