﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrolling : MonoBehaviour
{

    private Rigidbody2D background;


    // Start is called before the first frame update
    void Start()
    {
        background = GetComponent<Rigidbody2D> ();
        background.velocity = new Vector2(GameController.instance.scrollSpeed,0);
    }

    // Update is called once per frame
    void Update()
    {
        //if(GameController.instance.gameOver == true)
        //{
        //    background.velocity = Vector2.zero;
        //}
    }
}
