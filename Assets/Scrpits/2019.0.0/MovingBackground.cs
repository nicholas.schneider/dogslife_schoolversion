﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBackground : MonoBehaviour
{

    private BoxCollider2D buildings;
    private float buildingHHorizontal;

    // Start is called before the first frame update
    void Start()
    {
        buildings = GetComponent<BoxCollider2D>();
        buildingHHorizontal = buildings.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x < -buildingHHorizontal)
        {

            repositionBackground();
        }
    }

    private void repositionBackground()
    {
        Vector2 groundoffset = new Vector2(buildingHHorizontal * 2, 0);
        transform.position = (Vector2)transform.position + groundoffset;
    }
}
